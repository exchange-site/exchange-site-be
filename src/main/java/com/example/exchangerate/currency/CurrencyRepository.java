package com.example.exchangerate.currency;

import com.example.exchangerate.currency.entity.CurrencyEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CurrencyRepository extends JpaRepository<CurrencyEntity, Long> {
    CurrencyEntity findFirstByCode(String code);
    boolean existsByCode(String code);
}
