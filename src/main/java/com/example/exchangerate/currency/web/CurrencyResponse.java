package com.example.exchangerate.currency.web;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@EqualsAndHashCode
@ToString
public class CurrencyResponse {
  private Long id;
  private String code;
  private String nameLithuanian;
  private String nameEnglish;
  private Boolean hasRates;
}
