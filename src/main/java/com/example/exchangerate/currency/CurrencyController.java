package com.example.exchangerate.currency;

import com.example.exchangerate.currency.util.CurrencyMapper;
import com.example.exchangerate.currency.web.CurrencyResponse;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/currency")
public class CurrencyController {
  private final CurrencyService service;

  public CurrencyController(CurrencyService service) {
    this.service = service;
  }

  @GetMapping()
  public List<CurrencyResponse> findAll() {
    return service.findAll().stream()
            .map(CurrencyMapper::toResponse)
            .collect(Collectors.toList());
  }

  @GetMapping("/current")
  public List<CurrencyResponse> fetchCurrentCurrencies() {
    return service.updateCurrencies().stream()
        .map(CurrencyMapper::toResponse)
        .collect(Collectors.toList());
  }
}
