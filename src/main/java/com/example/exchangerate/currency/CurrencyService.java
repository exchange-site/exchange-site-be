package com.example.exchangerate.currency;

import com.example.exchangerate.currency.entity.CurrencyEntity;
import com.example.exchangerate.currency.util.CurrencyMapper;
import com.example.exchangerate.fxrate.FXRateService;
import com.example.exchangerate.fxrate.web.response.AllCurrenciesResponse;
import java.util.List;
import java.util.stream.Collectors;
import javax.transaction.Transactional;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class CurrencyService {
  private final CurrencyRepository repository;
  private final FXRateService fxRateService;

  public CurrencyService(CurrencyRepository repository, FXRateService fxRateService) {
    this.repository = repository;
    this.fxRateService = fxRateService;
  }

  public List<CurrencyEntity> findAll() {
    return repository.findAll();
  }

  public CurrencyEntity findByCode(String code) {
    if (!existsByCode(code)) {
      throw new RuntimeException("err.currency.codeNotFound code:" + code);
    }
    return repository.findFirstByCode(code);
  }

  public List<CurrencyEntity> updateCurrencies() {
    AllCurrenciesResponse response = fxRateService.getCurrencyList();
    if (response == null) {
      throw new RuntimeException("err.rate.currenciesUpdateFailed");
    }

    List<CurrencyEntity> persistedCurrencies = findAll();
    List<CurrencyEntity> notPersistedCurrencies =
        response.getCurrencies().stream()
            .map(CurrencyMapper::toEntity)
            .filter(currency -> !persistedCurrencies.contains(currency))
            .collect(Collectors.toList());
    if (notPersistedCurrencies.size() == 0) {
      return persistedCurrencies;
    }
    return repository.saveAll(notPersistedCurrencies);
  }

  public boolean existsByCode(String code) {
    return repository.existsByCode(code);
  }
}
