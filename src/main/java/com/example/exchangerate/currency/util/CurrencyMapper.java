package com.example.exchangerate.currency.util;

import com.example.exchangerate.currency.entity.CurrencyEntity;
import com.example.exchangerate.currency.web.CurrencyResponse;
import com.example.exchangerate.fxrate.web.entries.Currency;
import com.example.exchangerate.fxrate.web.entries.CurrencyName;
import com.example.exchangerate.fxrate.web.enums.Language;

public class CurrencyMapper {
  public static CurrencyResponse toResponse(CurrencyEntity entity) {
    if (entity == null) return null;
    return new CurrencyResponse(
        entity.getId(),
        entity.getCode(),
        entity.getNameLithuanian(),
        entity.getNameEnglish(),
            true);
  }

  public static CurrencyEntity toEntity(Currency currency) {
    CurrencyEntity entity = new CurrencyEntity();
    entity.setCode(currency.getCode());
    entity.setNumber(currency.getNumber());
    entity.setUnits(currency.getUnits());
    for (CurrencyName name : currency.getNames()) {
      if (name.getLang().equals(Language.EN)) {
        entity.setNameEnglish(name.getName());
      } else {
        entity.setNameLithuanian(name.getName());
      }
    }
    return entity;
  }
}
