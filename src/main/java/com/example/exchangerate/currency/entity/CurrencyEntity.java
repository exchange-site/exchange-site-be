package com.example.exchangerate.currency.entity;

import com.example.exchangerate.rate.entity.RateEntity;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.persistence.*;

@Entity()
@Table(name = "currency")
public class CurrencyEntity {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private String code;
  private String number;
  private String units;
  private String nameLithuanian;
  private String nameEnglish;

  @OneToMany(cascade = CascadeType.ALL, mappedBy = "rightCurrency", orphanRemoval = true)
  private List<RateEntity> rightRates = new ArrayList<>();

  @OneToMany(cascade = CascadeType.ALL, mappedBy = "leftCurrency", orphanRemoval = true)
  private List<RateEntity> leftRates = new ArrayList<>();

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getNumber() {
    return number;
  }

  public void setNumber(String number) {
    this.number = number;
  }

  public String getUnits() {
    return units;
  }

  public void setUnits(String units) {
    this.units = units;
  }

  public String getNameLithuanian() {
    return nameLithuanian;
  }

  public void setNameLithuanian(String nameLithuanian) {
    this.nameLithuanian = nameLithuanian;
  }

  public String getNameEnglish() {
    return nameEnglish;
  }

  public void setNameEnglish(String nameEnglish) {
    this.nameEnglish = nameEnglish;
  }

  public List<RateEntity> getRightRates() {
    return rightRates;
  }

  public void setRightRates(List<RateEntity> rightRates) {
    this.rightRates = rightRates;
  }

  public List<RateEntity> getLeftRates() {
    return leftRates;
  }

  public void setLeftRates(List<RateEntity> leftRates) {
    this.leftRates = leftRates;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof CurrencyEntity)) return false;
    CurrencyEntity entity = (CurrencyEntity) o;
    return Objects.equals(getCode(), entity.getCode())
        && Objects.equals(getNumber(), entity.getNumber())
        && Objects.equals(getUnits(), entity.getUnits())
        && Objects.equals(getNameLithuanian(), entity.getNameLithuanian())
        && Objects.equals(getNameEnglish(), entity.getNameEnglish());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getCode(), getNumber(), getUnits(), getNameLithuanian(), getNameEnglish());
  }
}
