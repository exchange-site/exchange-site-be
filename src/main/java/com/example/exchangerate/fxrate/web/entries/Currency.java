package com.example.exchangerate.fxrate.web.entries;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.*;

@XmlRootElement(name = "CcyNtry", namespace = "http://www.lb.lt/WebServices/FxRates")
@XmlAccessorType(XmlAccessType.FIELD)
public class Currency {
  @XmlElement(name = "Ccy", namespace = "http://www.lb.lt/WebServices/FxRates")
  private String code;

  @XmlElements({@XmlElement(name = "CcyNm", namespace = "http://www.lb.lt/WebServices/FxRates")})
  private List<CurrencyName> names = new ArrayList<>();

  @XmlElement(name = "CcyNbr", namespace = "http://www.lb.lt/WebServices/FxRates")
  private String number;

  @XmlElement(name = "CcyMnrUnts", namespace = "http://www.lb.lt/WebServices/FxRates")
  private String units;

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public List<CurrencyName> getNames() {
    return names;
  }

  public void setNames(List<CurrencyName> names) {
    this.names = names;
  }

  public String getNumber() {
    return number;
  }

  public void setNumber(String number) {
    this.number = number;
  }

  public String getUnits() {
    return units;
  }

  public void setUnits(String units) {
    this.units = units;
  }
}
