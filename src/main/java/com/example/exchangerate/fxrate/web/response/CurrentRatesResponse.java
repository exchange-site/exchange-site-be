package com.example.exchangerate.fxrate.web.response;

import com.example.exchangerate.fxrate.web.entries.FxRate;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "FxRates", namespace = "http://www.lb.lt/WebServices/FxRates")
@XmlAccessorType(XmlAccessType.FIELD)
public class CurrentRatesResponse {
  @XmlElement(name = "FxRate", namespace = "http://www.lb.lt/WebServices/FxRates")
  private List<FxRate> rates = new ArrayList<>();

  public List<FxRate> getRates() {
    return rates;
  }

  public void setRates(List<FxRate> rates) {
    this.rates = rates;
  }
}
