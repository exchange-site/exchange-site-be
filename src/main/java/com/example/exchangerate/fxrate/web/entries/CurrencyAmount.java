package com.example.exchangerate.fxrate.web.entries;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = "CcyAmt", namespace = "http://www.lb.lt/WebServices/FxRates")
@XmlAccessorType(XmlAccessType.FIELD)
public class CurrencyAmount {
  @XmlElement(name = "Ccy", namespace = "http://www.lb.lt/WebServices/FxRates")
  private String name;

  @XmlElement(name = "Amt", namespace = "http://www.lb.lt/WebServices/FxRates")
  private String amount;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getAmount() {
    return amount;
  }

  public void setAmount(String amount) {
    this.amount = amount;
  }
}
