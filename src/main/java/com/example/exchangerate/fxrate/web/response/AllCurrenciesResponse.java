package com.example.exchangerate.fxrate.web.response;

import com.example.exchangerate.fxrate.web.entries.Currency;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.*;

@XmlRootElement(name = "CcyTbl", namespace = "http://www.lb.lt/WebServices/FxRates")
@XmlAccessorType(XmlAccessType.FIELD)
public class AllCurrenciesResponse {
  @XmlElement(name = "CcyNtry", namespace = "http://www.lb.lt/WebServices/FxRates")
  private List<Currency> currencies = new ArrayList<>();

  public List<Currency> getCurrencies() {
    return currencies;
  }

  public void setCurrencies(List<Currency> currencies) {
    this.currencies = currencies;
  }
}
