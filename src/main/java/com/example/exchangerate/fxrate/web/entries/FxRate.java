package com.example.exchangerate.fxrate.web.entries;

import com.example.exchangerate.fxrate.web.enums.CourseType;
import com.example.exchangerate.util.adapter.LocalDateAdapter;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlRootElement(name = "FxRate", namespace = "http://www.lb.lt/WebServices/FxRates")
@XmlAccessorType(XmlAccessType.FIELD)
public class FxRate {
  @XmlElement(name = "Tp", namespace = "http://www.lb.lt/WebServices/FxRates")
  private CourseType type;

  @XmlElement(name = "Dt", namespace = "http://www.lb.lt/WebServices/FxRates")
  @XmlJavaTypeAdapter(LocalDateAdapter.class)
  private LocalDate date;

  @XmlElements({@XmlElement(name = "CcyAmt", namespace = "http://www.lb.lt/WebServices/FxRates")})
  private List<CurrencyAmount> amounts = new ArrayList<>();

  public CourseType getType() {
    return type;
  }

  public void setType(CourseType type) {
    this.type = type;
  }

  public LocalDate getDate() {
    return date;
  }

  public void setDate(LocalDate date) {
    this.date = date;
  }

  public List<CurrencyAmount> getAmounts() {
    return amounts;
  }

  public void setAmounts(List<CurrencyAmount> amounts) {
    this.amounts = amounts;
  }
}
