package com.example.exchangerate.fxrate.web.entries;

import com.example.exchangerate.fxrate.web.enums.Language;
import javax.xml.bind.annotation.*;

@XmlRootElement(name = "CcyNm", namespace = "http://www.lb.lt/WebServices/FxRates")
@XmlAccessorType(XmlAccessType.FIELD)
public class CurrencyName {
  @XmlAttribute private Language lang;
  @XmlValue private String name;

  public Language getLang() {
    return lang;
  }

  public void setLang(Language lang) {
    this.lang = lang;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}
