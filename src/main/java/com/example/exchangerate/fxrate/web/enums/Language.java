package com.example.exchangerate.fxrate.web.enums;

public enum Language {
    LT, EN
}
