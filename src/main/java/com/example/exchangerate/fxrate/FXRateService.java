package com.example.exchangerate.fxrate;

import com.example.exchangerate.fxrate.web.enums.CourseType;
import com.example.exchangerate.fxrate.web.response.AllCurrenciesResponse;
import com.example.exchangerate.fxrate.web.response.CurrentRatesResponse;
import com.example.exchangerate.util.XMLReaderUtil;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Service
public class FXRateService {
  private final RestTemplate restTemplate;

  @Value("${application.fxrates.endpoints.currentRate}")
  private String CURRENT_RATE;

  @Value("${application.fxrates.endpoints.rate}")
  private String RATE;

  @Value("${application.fxrates.endpoints.rateByCurrency}")
  private String RATE_BY_CURRENCY;

  @Value("${application.fxrates.endpoints.currencyList}")
  private String CURRENCY_LIST;

  public FXRateService(RestTemplate restTemplate) {
    this.restTemplate = restTemplate;
  }

  @Nullable
  public CurrentRatesResponse getCurrentFxRates(CourseType type) {
    String urlTemplate =
        UriComponentsBuilder.fromHttpUrl(CURRENT_RATE)
            .queryParam("tp", "{tp}")
            .encode()
            .toUriString();

    Map<String, String> params = new HashMap<>();
    if (type != null) {
      params.put("tp", type.toString());
    }

    String response = getForEntity(urlTemplate, params);
    return XMLReaderUtil.getResult(response, CurrentRatesResponse.class);
  }

  @Nullable
  public CurrentRatesResponse getFxRates(@Nullable CourseType type, @Nullable LocalDate date) {
    String urlTemplate =
        UriComponentsBuilder.fromHttpUrl(RATE)
            .queryParam("tp", "{tp}")
            .queryParam("dt", "{dt}")
            .encode()
            .toUriString();

    Map<String, String> params = new HashMap<>();
    if (type != null) {
      params.put("tp", type.toString());
    }
    if (date != null) {
      params.put("dt", date.toString());
    }

    String response = getForEntity(urlTemplate, params);
    return XMLReaderUtil.getResult(response, CurrentRatesResponse.class);
  }

  @Nullable
  public CurrentRatesResponse getFxRatesForCurrency(
      @Nullable CourseType type,
      @Nullable String currency,
      @Nullable LocalDate from,
      @Nullable LocalDate to) {
    String urlTemplate =
        UriComponentsBuilder.fromHttpUrl(RATE_BY_CURRENCY)
            .queryParam("tp", "{tp}")
            .queryParam("ccy", "{ccy}")
            .queryParam("dtFrom", "{dtFrom}")
            .queryParam("dtTo", "{dtTo}")
            .encode()
            .toUriString();

    Map<String, String> params = new HashMap<>();
    if (type != null) {
      params.put("tp", type.toString());
    }
    if (currency != null) {
      params.put("ccy", currency);
    }
    if (from != null) {
      params.put("dtFrom", from.toString());
    }
    if (to != null) {
      params.put("dtTo", to.toString());
    }

    String response = getForEntity(urlTemplate, params);
    return XMLReaderUtil.getResult(response, CurrentRatesResponse.class);
  }

  @Nullable
  public AllCurrenciesResponse getCurrencyList() {
    String url = CURRENCY_LIST;
    String response = getForEntity(url, null);
    return XMLReaderUtil.getResult(response, AllCurrenciesResponse.class);
  }

  private String getForEntity(String urlTemplate, @Nullable Map<String, String> params) {
    HttpHeaders headers = new HttpHeaders();
    headers.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);

    if (params != null && !params.isEmpty()) {
      return restTemplate
          .exchange(urlTemplate, HttpMethod.GET, new HttpEntity<>(headers), String.class, params)
          .getBody();
    }
    return restTemplate
        .exchange(urlTemplate, HttpMethod.GET, new HttpEntity<>(headers), String.class)
        .getBody();
  }
}
