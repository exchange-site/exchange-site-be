package com.example.exchangerate.rate;

import com.example.exchangerate.rate.entity.RateEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RateRepository extends JpaRepository<RateEntity, Long> {
    List<RateEntity> findAllByRightCurrencyId(Long id);
}
