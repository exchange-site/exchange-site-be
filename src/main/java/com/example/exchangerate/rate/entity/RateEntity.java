package com.example.exchangerate.rate.entity;

import com.example.exchangerate.currency.entity.CurrencyEntity;
import com.example.exchangerate.fxrate.web.enums.CourseType;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;
import javax.persistence.*;

@Entity()
@Table(name = "rate")
public class RateEntity {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Enumerated(value = EnumType.STRING)
  private CourseType type;

  private LocalDate date;

  @Column(name = "l_amount")
  private BigDecimal leftAmount;

  @Column(name = "r_amount")
  private BigDecimal rightAmount;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "l_currency", referencedColumnName = "id")
  private CurrencyEntity leftCurrency;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "r_currency", referencedColumnName = "id")
  private CurrencyEntity rightCurrency;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public CourseType getType() {
    return type;
  }

  public void setType(CourseType type) {
    this.type = type;
  }

  public LocalDate getDate() {
    return date;
  }

  public void setDate(LocalDate date) {
    this.date = date;
  }

  public BigDecimal getLeftAmount() {
    return leftAmount;
  }

  public void setLeftAmount(BigDecimal lAmount) {
    this.leftAmount = lAmount;
  }

  public BigDecimal getRightAmount() {
    return rightAmount;
  }

  public void setRightAmount(BigDecimal rAmount) {
    this.rightAmount = rAmount;
  }

  public CurrencyEntity getLeftCurrency() {
    return leftCurrency;
  }

  public void setLeftCurrency(CurrencyEntity lCurrency) {
    this.leftCurrency = lCurrency;
  }

  public CurrencyEntity getRightCurrency() {
    return rightCurrency;
  }

  public void setRightCurrency(CurrencyEntity rightCurrency) {
    this.rightCurrency = rightCurrency;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof RateEntity)) return false;
    RateEntity that = (RateEntity) o;
    return getType() == that.getType()
        && Objects.equals(getDate(), that.getDate())
        && Objects.equals(getLeftAmount(), that.getLeftAmount())
        && Objects.equals(getRightAmount(), that.getRightAmount())
        && Objects.equals(getLeftCurrency(), that.getLeftCurrency())
        && Objects.equals(getRightCurrency(), that.getRightCurrency());
  }

  @Override
  public int hashCode() {
    return Objects.hash(
        getType(),
        getDate(),
        getLeftAmount(),
        getRightAmount(),
        getLeftCurrency(),
        getRightCurrency());
  }
}
