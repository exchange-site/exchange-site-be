package com.example.exchangerate.rate;

import com.example.exchangerate.rate.util.RateMapper;
import com.example.exchangerate.rate.web.CalculationResponse;
import com.example.exchangerate.rate.web.RateResponse;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/rate")
public class RateController {
  private final RateService service;

  public RateController(RateService service) {
    this.service = service;
  }

  @GetMapping
  public List<RateResponse> findAll() {
    return RateMapper.toResponses(service.findAll());
  }

  @GetMapping("/current")
  public List<RateResponse> fetchCurrentRates() {
    return RateMapper.toResponses(service.fetchCurrentRates());
  }

  @GetMapping("/rate")
  public List<RateResponse> getRatesForCurrency(
      @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate from,
      @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate to,
      @RequestParam @Length(min = 3, max = 3) String currency) {
    return RateMapper.toSortedByDateResponses(service.findRates(from, to, currency));
  }

  @GetMapping("/calculate")
  public CalculationResponse calculateRate(
      @RequestParam String currencyFrom,
      @RequestParam String currencyTo,
      @RequestParam @Length(min = 1, max = 23) String amount) {
    return service.calculate(currencyFrom, currencyTo, new BigDecimal(amount));
  }
}
