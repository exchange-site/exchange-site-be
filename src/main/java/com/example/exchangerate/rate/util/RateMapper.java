package com.example.exchangerate.rate.util;

import com.example.exchangerate.currency.entity.CurrencyEntity;
import com.example.exchangerate.currency.util.CurrencyMapper;
import com.example.exchangerate.fxrate.web.entries.CurrencyAmount;
import com.example.exchangerate.fxrate.web.entries.FxRate;
import com.example.exchangerate.rate.entity.RateEntity;
import com.example.exchangerate.rate.web.CalculationResponse;
import com.example.exchangerate.rate.web.RateResponse;
import java.math.BigDecimal;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class RateMapper {
  public static CalculationResponse toCalculationResponse(
      CurrencyEntity from, BigDecimal fromAmount, CurrencyEntity to, BigDecimal toAmount) {
    return new CalculationResponse(
        fromAmount, CurrencyMapper.toResponse(from), toAmount, CurrencyMapper.toResponse(to));
  }

  public static List<RateResponse> toResponses(List<RateEntity> entities) {
    return entities.parallelStream().map(RateMapper::toResponse).collect(Collectors.toList());
  }

  public static List<RateResponse> toSortedByDateResponses(List<RateEntity> entities) {
    return entities.parallelStream()
        .map(RateMapper::toResponse)
        .sorted(Comparator.comparing(RateResponse::getDate))
        .collect(Collectors.toList());
  }

  public static RateResponse toResponse(RateEntity entity) {
    if (entity == null) return null;
    return new RateResponse(
        entity.getId(),
        entity.getType(),
        entity.getDate(),
        entity.getLeftAmount(),
        CurrencyMapper.toResponse(entity.getLeftCurrency()),
        entity.getRightAmount(),
        CurrencyMapper.toResponse(entity.getRightCurrency()));
  }

  public static RateEntity toEntity(FxRate fxRate, List<CurrencyEntity> currencies) {
    RateEntity entity = new RateEntity();
    entity.setType(fxRate.getType());
    entity.setDate(fxRate.getDate());
    for (CurrencyAmount amount : fxRate.getAmounts()) {
      CurrencyEntity currency =
          currencies.stream()
              .filter(c -> c.getCode().equals(amount.getName()))
              .findFirst()
              .orElseThrow(() -> new RuntimeException("err.rate.cantFindCurrency"));

      BigDecimal rate = new BigDecimal(amount.getAmount());
      if (rate.equals(BigDecimal.ONE)) {
        entity.setLeftAmount(BigDecimal.valueOf(1.));
        entity.setLeftCurrency(currency);
      } else {
        entity.setRightAmount(new BigDecimal(amount.getAmount()));
        entity.setRightCurrency(currency);
      }
    }
    return entity;
  }
}
