package com.example.exchangerate.rate;

import com.example.exchangerate.currency.CurrencyService;
import com.example.exchangerate.currency.entity.CurrencyEntity;
import com.example.exchangerate.fxrate.FXRateService;
import com.example.exchangerate.fxrate.web.enums.CourseType;
import com.example.exchangerate.fxrate.web.response.CurrentRatesResponse;
import com.example.exchangerate.rate.entity.RateEntity;
import com.example.exchangerate.rate.util.RateMapper;
import com.example.exchangerate.rate.web.CalculationResponse;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class RateService {
  private final RateRepository repository;
  private final CurrencyService currencyService;
  private final FXRateService fxRateService;

  public RateService(
      RateRepository repository, CurrencyService currencyService, FXRateService fxRateService) {
    this.repository = repository;
    this.currencyService = currencyService;
    this.fxRateService = fxRateService;
  }

  public List<RateEntity> findAll() {
    return repository.findAll();
  }

  public RateEntity findLatestByCurrencyId(Long id) {
    List<RateEntity> toEntities = repository.findAllByRightCurrencyId(id);
    if (toEntities.size() == 0) {
      throw new RuntimeException("err.rate.calculation.rateFetchingFailed");
    }
    return toEntities.get(0);
  }

  public List<RateEntity> fetchCurrentRates() {
    CurrentRatesResponse response = fxRateService.getCurrentFxRates(CourseType.EU);
    if (response == null) {
      throw new RuntimeException("err.rate.dataFetchingFailed");
    }

    List<CurrencyEntity> currencies = currencyService.findAll();
    List<RateEntity> persisted = findAll();
    List<RateEntity> notPersisted =
        response.getRates().stream()
            .map(fxRate -> RateMapper.toEntity(fxRate, currencies))
            .filter(rate -> persisted.stream().noneMatch(rate::equals))
            .collect(Collectors.toList());

    if (notPersisted.size() != 0) {
      repository.saveAll(notPersisted);
    }
    return findAll();
  }

  public List<RateEntity> findRates(LocalDate from, LocalDate to, String currencyCode) {
    if (!currencyService.existsByCode(currencyCode)) {
      throw new RuntimeException("err.rate.currencyCodeNotFound code:" + currencyCode);
    }
    CurrentRatesResponse response =
        fxRateService.getFxRatesForCurrency(CourseType.EU, currencyCode, from, to);
    if (response == null) {
      throw new RuntimeException("err.rate.dataFetchingFailed");
    }

    List<CurrencyEntity> currencies = currencyService.findAll();
    return response.getRates().parallelStream()
        .map(fxRate -> RateMapper.toEntity(fxRate, currencies))
        .collect(Collectors.toList());
  }

  public CalculationResponse calculate(String currencyFrom, String currencyTo, BigDecimal amount) {
    MathContext roundingContext = new MathContext(amount.precision() + 8, RoundingMode.HALF_UP);
    CurrencyEntity from = currencyService.findByCode(currencyFrom);
    CurrencyEntity to = currencyService.findByCode(currencyTo);
    BigDecimal toAmount = amount;

    if (Objects.equals(currencyFrom, "EUR") && !Objects.equals(currencyTo, "EUR")) {
      RateEntity toRate = findLatestByCurrencyId(to.getId());
      toAmount = amount.multiply(toRate.getRightAmount());
    } else if (!Objects.equals(currencyFrom, "EUR") && Objects.equals(currencyTo, "EUR")) {
      RateEntity fromRate = findLatestByCurrencyId(from.getId());
      toAmount = amount.divide(fromRate.getRightAmount(), roundingContext);
    } else if (!Objects.equals(currencyFrom, currencyTo)) {
      RateEntity toRate = findLatestByCurrencyId(to.getId());
      RateEntity fromRate = findLatestByCurrencyId(from.getId());
      toAmount =
          amount
              .divide(fromRate.getRightAmount(), roundingContext)
              .multiply(toRate.getRightAmount());
    }
    return RateMapper.toCalculationResponse(from, amount, to, toAmount);
  }
}
