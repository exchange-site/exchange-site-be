package com.example.exchangerate.rate.web;

import com.example.exchangerate.currency.web.CurrencyResponse;
import com.example.exchangerate.fxrate.web.enums.CourseType;
import java.math.BigDecimal;
import java.time.LocalDate;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@EqualsAndHashCode
@ToString
public class RateResponse {
  private Long id;
  private CourseType type;
  private LocalDate date;
  private BigDecimal leftAmount;
  private CurrencyResponse leftCurrency;
  private BigDecimal rightAmount;
  private CurrencyResponse rightCurrency;
}
