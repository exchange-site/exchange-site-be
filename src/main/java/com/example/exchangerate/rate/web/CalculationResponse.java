package com.example.exchangerate.rate.web;

import com.example.exchangerate.currency.web.CurrencyResponse;
import java.math.BigDecimal;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@EqualsAndHashCode
@ToString
public class CalculationResponse {
  private BigDecimal leftAmount;
  private CurrencyResponse leftCurrency;
  private BigDecimal rightAmount;
  private CurrencyResponse rightCurrency;
}
