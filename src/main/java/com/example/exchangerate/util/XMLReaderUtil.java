package com.example.exchangerate.util;

import java.io.StringReader;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

public class XMLReaderUtil {
  public static <T> T getResult(String xmlString, Class<T> resultClass) {
    StringReader xsr = new StringReader(xmlString);
    try {
      JAXBContext jaxbContext = JAXBContext.newInstance(resultClass);
      Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
      return resultClass.cast(unmarshaller.unmarshal(xsr));
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }
}
