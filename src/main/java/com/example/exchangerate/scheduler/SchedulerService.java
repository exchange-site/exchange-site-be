package com.example.exchangerate.scheduler;

import com.example.exchangerate.currency.CurrencyService;
import com.example.exchangerate.rate.RateService;
import java.util.List;
import javax.transaction.Transactional;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class SchedulerService {
  private final Logger LOGGER = LogManager.getLogger(SchedulerService.class);

  private final RateService rateService;
  private final CurrencyService currencyService;

  public SchedulerService(RateService rateService, CurrencyService currencyService) {
    this.rateService = rateService;
    this.currencyService = currencyService;
  }

  public void getDailyCurrencyRates() {
    LOGGER.info("Fetching daily currency rates. Start.");
    List<?> rates = rateService.fetchCurrentRates();
    LOGGER.info("Fetching daily currency rates. End. Fetched {} rate entries", rates.size());
  }

  public void updateCurrencyList() {
    LOGGER.info("Updating currency list. Start.");
    List<?> currencies = currencyService.updateCurrencies();
    LOGGER.info("Updating currency list. End. Fetcher {} currency entries.", currencies.size());
  }
}
