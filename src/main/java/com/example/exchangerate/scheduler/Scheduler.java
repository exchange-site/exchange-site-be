package com.example.exchangerate.scheduler;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class Scheduler {
  private final SchedulerService service;

  public Scheduler(SchedulerService service) {
    this.service = service;
  }

  @Scheduled(cron = "${application.cron.currentRate}", zone = "Europe/Vilnius")
  public void getDailyCurrencyRates() {
    service.getDailyCurrencyRates();
  }

  @Scheduled(cron = "${application.cron.currencyList}", zone = "Europe/Vilnius")
  public void updateCurrencyList() {
    service.updateCurrencyList();
  }
}
