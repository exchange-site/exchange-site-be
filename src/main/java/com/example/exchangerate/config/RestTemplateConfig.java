package com.example.exchangerate.config;

import java.time.Duration;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class RestTemplateConfig {
  @Bean()
  public RestTemplate restTemplate() {
    Duration timeout = Duration.ofMinutes(2);
    return new RestTemplateBuilder().setReadTimeout(timeout).build();
  }
}
