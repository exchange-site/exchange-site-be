package com.example.exchangerate.config;

import com.example.exchangerate.currency.CurrencyService;
import com.example.exchangerate.rate.RateService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class AfterApplicationInit {
    private final Logger LOGGER = LogManager.getLogger(AfterApplicationInit.class);

    private final RateService rateService;
    private final CurrencyService currencyService;

    public AfterApplicationInit(RateService rateService, CurrencyService currencyService) {
        this.rateService = rateService;
        this.currencyService = currencyService;
    }

    @EventListener
    public void onApplicationEvent(ContextRefreshedEvent event) {
        LOGGER.info("Fetching initial data for currencies and currency rates.");
        currencyService.updateCurrencies();
        rateService.fetchCurrentRates();
        LOGGER.info("Fetching initial data completed.");
    }
}
