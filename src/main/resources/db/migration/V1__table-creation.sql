CREATE TABLE IF NOT EXISTS currency
(
    id              SERIAL PRIMARY KEY,
    code            TEXT NOT NULL,
    number          TEXT NOT NULL,
    units           TEXT NOT NULL,
    name_lithuanian TEXT NOT NULL,
    name_english    TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS rate
(
    id         SERIAL PRIMARY KEY,
    type       TEXT      NOT NULL,
    date       TIMESTAMP NOT NULL,
    l_amount   DOUBLE    NOT NULL,
    l_currency INT       NOT NULL,
    r_amount   DOUBLE    NOT NULL,
    r_currency INT       NOT NULL,

    FOREIGN KEY (l_currency) REFERENCES currency (id),
    FOREIGN KEY (r_currency) REFERENCES currency (id)
);